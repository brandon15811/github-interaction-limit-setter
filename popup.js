//Based on: https://stackoverflow.com/a/44864966/1951549
function createTab(url) {
    return new Promise(resolve => {
        chrome.tabs.create({ 'url': url }, async tab => {
            await waitForTabLoad(tab.id);
            resolve(tab);
        });
    });
}

//Based on: https://stackoverflow.com/a/44864966/1951549
function updateTabUrl(tabIdToUpdate, url) {
    return new Promise(resolve => {
        chrome.tabs.update(tabIdToUpdate, { 'url': url }, async tab => {
            await waitForTabLoad(tab.id);
            resolve(tab);
        });
    });
}

//Based on: https://stackoverflow.com/a/44864966/1951549
function waitForTabLoad(tabIdToWait) {
    return new Promise(resolve => {
        chrome.tabs.onUpdated.addListener(function listener(tabId, info) {
            if (info.status === 'complete' && tabId === tabIdToWait) {
                chrome.tabs.onUpdated.removeListener(listener);
                resolve();
            }
        });
    });
}

function executeCode(tabId, code) {
    return new Promise(resolve => {
        chrome.tabs.executeScript(tabId, {
            code: code
        }, (result) => resolve(result[0]));
    });
}

async function getButtonText(tabId, selector) {
    return await executeCode(tabId, `document.querySelector('${selector}').innerText;`);
}

async function scrollToButton(tabId, selector) {
    return await executeCode(tabId, `document.querySelector('${selector}').scrollIntoViewIfNeeded()`);
}

async function clickButton(tabId, selector) {
    await scrollToButton(tabId, selector);
    return await executeCode(tabId, `let button = document.querySelector('${selector}').click();`);
}

function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    });
}

function $(id) {
    return document.querySelector(id);
}

function log(str) {
    $('#log').insertAdjacentText('afterbegin', str + '\n');
}


async function run() {
    let githubSlug = $('#githubSlug').value;
    if (githubSlug === '') return;
    let githubType = $('#isUser').checked ? 'users' : 'orgs';
    let limitType = '#' + $('#limitType').value;

    //let urls = ['https://github.com/psychic-waddle/automatic-octo-giggle/settings', 'https://github.com/psychic-waddle/sturdy-pancake/settings'];

    let res = await fetch(`https://api.github.com/${githubType}/${githubSlug}/repos`)
    if (!res.ok) {
        //types.forEach((type) => $(type).value = 'Github API Error: ' + res.statusText);
        log('Github API Error: ' + res.statusText);
        return;
    }

    let repos = await res.json();
    let repoList = repos.filter(x => x.fork === false).map(x => x.name);

    let currentTab = await createTab('about:blank');
    let tabId = currentTab.id;

    for (let i = 0; i < repoList.length; i++) {
        let currentRepo = `${githubSlug}/${repoList[i]}`;
        let url = `https://github.com/${currentRepo}/settings`

        await updateTabUrl(tabId, url);
        if (await getButtonText(tabId, limitType) === 'Disable') {
            log(`Disabling ${limitType} limits on ${currentRepo}`);
            await clickButton(tabId, limitType);
            await waitForTabLoad(tabId);
            await scrollToButton(tabId, limitType);
            sleep(1000);
        }
        log(`Enabling ${limitType} limits on ${currentRepo}`);
        await clickButton(tabId, limitType);
        await waitForTabLoad(tabId);
        await scrollToButton(tabId, limitType);
        sleep(1000);
    }
}

window.onload = () => {
    $('#githubForm').onsubmit = (e) => {
        e.preventDefault();
        run();
    }
}