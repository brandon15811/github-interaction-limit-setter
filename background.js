chrome.browserAction.onClicked.addListener(function() {
    chrome.windows.create({
        url: chrome.extension.getURL('popup.html'),
        focused: true,
        type: "popup",
        width: 640,
        height: 480
    });
});